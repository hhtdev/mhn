import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';
import { IonSlides} from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

    @ViewChild('mySlider', {static: false}) slider: IonSlides;

    slideOpts = {
        initialSlide: 0,
        speed: 5000
    };
  public resultat: any = 0;
  public appareils = [
      {
            appareil : 'Avion',
            emission : '16000',
            genre    : 'masculin',
            src : 'assets/icon/plane.svg'
      },
      {
          appareil : 'Ampoule',
          emission : '2',
          genre    : 'feminin',
          src : 'assets/icon/ampoule.svg'
      },
      {
          appareil : 'Fusée',
          emission : '336000',
          genre    : 'feminin',
          src : 'assets/icon/rocket.svg'
      }
    ];
  constructor() {}

    slidesDidLoad(slides: IonSlides) {
        slides.startAutoplay();
    }
 calculEmission(paramEmission) {
    var emission;
    emission = new Intl.NumberFormat('fr-FR').format(this.resultat / paramEmission);
    return emission;
  }

  mailEmission() {
    var resultat = this.resultat;
    var email = resultat * 10;
    return email;
  }

  mailSend() {
    var mail;
    mail = this.resultat;
    mail.toFixed(2);
    return mail;
  }
}
